'''Code to extract double zipped files'''

from __future__ import print_function
import sys, os
import glob
import zipfile
import re
import time

# Progress_bar
class ProgressBar(object):
    DEFAULT = 'Progress: %(bar)s %(percent)3d%%'
    FULL = '%(bar)s %(current)d/%(total)d (%(percent)3d%%) %(remaining)d to go'

    def __init__(self, total, width=40, fmt=DEFAULT, symbol='=',
                 output=sys.stderr):
        assert len(symbol) == 1

        self.total = total
        self.width = width
        self.symbol = symbol
        self.output = output
        self.fmt = re.sub(r'(?P<name>%\(.+?\))d',
                          r'\g<name>%dd' % len(str(total)), fmt)

        self.current = 0

    def __call__(self):
        percent = self.current / float(self.total)
        size = int(self.width * percent)
        remaining = self.total - self.current
        bar = '[' + self.symbol * size + ' ' * (self.width - size) + ']'

        args = {
            'total': self.total,
            'bar': bar,
            'current': self.current,
            'percent': percent * 100,
            'remaining': remaining
        }
        print('\r' + self.fmt % args, file=self.output, end='')

    def done(self):
        self.current = self.total
        self()
        print('', file=self.output)



def extract_files(_new_folder_name,file_count):
    #append the ./ * / to create the proper path
    new_folder_name = _new_folder_name + '/'

    count = 1

    # Loop through all zipped files in the folder
    for filename in glob.iglob(path + '*.zip'):
        #print('Processing file:' + '%s' % filename)

        #print the current progress
        progress.current += 1
        progress()

        # Level 1: first unzip
        zip_ref = zipfile.ZipFile(filename, 'r')
        zip_ref.extractall(new_folder_name)
        zip_ref.close()
        
        #extracted zip file:
        filename1 = glob.glob(new_folder_name + '*.zip')[0]

        #to avoid name conflict of submitted files create a folder with the students name
        _ext_folder=filename[len(_new_folder_name)-len('_extracted')+3:-4] #the filename can be extracted from the MyITlab

        #format the name
        ext_folder = _ext_folder.split('--')[0]+'_'+_ext_folder.split('--')[2]


        # in case a folder with the same name exist
        if (os.path.isdir(new_folder_name+ext_folder)):
            print('Folder named '+ new_folder_name+ext_folder + ' already exists!')
        else:
            os.mkdir(new_folder_name+ext_folder)
            

        

        #Level 2: second unzip
        zip_ref = zipfile.ZipFile(filename1, 'r')
        zip_ref.extractall(new_folder_name+ext_folder) #extract in the new folder
        zip_ref.close()

        #remove the extracted zip file from Level 1
        os.remove(filename1)

        #now left with only the *.xlsx file

        count = count + 1
        #print the processed file
        print(' Processed: ' + str(filename[2:-4]))

    print('\n')
    print(str(count)+'/'+str(file_count)+' files are processed.')


if __name__ == "__main__":

    
    start_time = time.time()
    for i in range(len(sys.argv)-1):

        #check the folder name given as input correct or not
        if not(os.path.isdir(str(sys.argv[i+1]))):
            print('Folder name '+ str(sys.argv[i+1])+ ' does not exit!')
            continue

        path = './'+str(sys.argv[i+1])+'/'

        print('\n')
        print('----------------------------------------------------------------------')
        print('Processing files inside:', path )
        print('----------------------------------------------------------------------')
        print('\n')
        #number of files:
        # code adjusted for version independence
        if sys.version_info[0] < 3:
            path, dirs, files = os.walk(path).next()
        else:
            path, dirs, files = os.walk(path).__next__()
        file_count = len(files)

        # print the progress bar
        progress = ProgressBar(file_count, fmt=ProgressBar.FULL)
        # create a new directory where the files are going to be extracted
        # for eg: path ='./Okos/' --> path.strip()[2:--1] --> 'Okos'
        # New folder named: Okos_extracted
        new_folder_name = path.strip()[2:-1] + '_extracted'


        # in case a folder with the same name exist
        if (os.path.isdir(new_folder_name)):
            print('Folder named '+ str(sys.argv[i+1]) + '_extracted already exists!')
        else:
            os.mkdir(new_folder_name)
            print('New folder named ' + new_folder_name + ' created')

        print('\n')
        # extract the file
        extract_files(new_folder_name, file_count)

    print('\n')
    print("--- %s seconds Elapsed ---" % (time.time() - start_time))
    print('-----------------------------Done-----------------------------------')
