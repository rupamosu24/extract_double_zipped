# Extract Double Zipped File
## Requirement: Python 2.7 or Python 3.6


1. This is a simple python script to execute a double layer unzipping.

2. Keep the code "extract_files.py" and the folders (from which the files will be extracted) in the same directory

3. For MAC/Linux users:
	
	a. Open terminal and navigate to the directory where the file and folder/folders are stored.
	
	b. Run "python extract_files.py <foldername> [<foldername2>]....."      ### NOTE: One or more folder names can be provided.
	
	c. The extracted files will be stored in a new folder named as <foldername>_extracted/<subfolder_student_name>

4. For Windows:

        a. Open command prompt (search--> cmd.exe)
        
        b. Run "python extract_files.py" <foldername> [<foldername2>]....." ### NOTE: One or more folder names can be provided.
        
        c. The extracted files will be stored in a new folder <foldername>_extracted/<subfolder_student_name>
        

5.  Additional tools:

    ## Python Installation in MAC:
        Direct download -- > https://www.python.org/downloads/
        Through terminal -- > https://zaiste.net/posts/installing_python_27_on_osx_with_homebrew/
        
    ## Python Installation in Windows:
        https://www.python.org/downloads/release/python-2714/
    
    ## Python Installation in Linux:
        https://stackoverflow.com/questions/6630873/how-to-download-python-from-command-line




## Sample Run:

### Case 1: Single folder

>>python extract_files.py Okos

----------------------------------------------------------------------
Processing files inside: ./Okos/
----------------------------------------------------------------------


New folder named Okos_extracted created


[======================================  ] 20/21 ( 95%)  1 to go

21/21 files are processed.


--- 0.0831701755524 seconds Elapsed ---
-----------------------------Done-----------------------------------

### Case 2: Multiple folders.
NOTE: Folder names with space in it should be in the format "*\ *". For eg: "Reid Furniture" should be written as "Reid\ Furniture"

>> python extract_files.py Okos Reid\ Furniture


----------------------------------------------------------------------
Processing files inside: ./Okos/
----------------------------------------------------------------------


New folder named Okos_extracted created


[======================================  ] 20/21 ( 95%)  1 to go

21/21 files are processed.


----------------------------------------------------------------------
Processing files inside: ./Reid Furniture/
----------------------------------------------------------------------


New folder named Reid Furniture_extracted created


[========================================] 17/17 (100%)  0 to go

17/17 files are processed.


--- 0.178990840912 seconds Elapsed ---
-----------------------------Done-----------------------------------

6. Feel free to modify and use.

